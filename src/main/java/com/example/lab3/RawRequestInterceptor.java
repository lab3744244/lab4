package com.example.lab3;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;

import java.io.*;
import java.security.SecureRandom;
import java.util.Enumeration;

@Component
public class RawRequestInterceptor implements HandlerInterceptor {


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

//        ContentCachingRequestWrapper cachingRequestWrapper = new ContentCachingRequestWrapper(request);
//        System.out.println("Is Content Cached: " + cachingRequestWrapper.getContentAsByteArray().toString());

//        System.out.println("Content Length: " + request.getContentLength());

//        String requestBody = readInputStream(cachingRequestWrapper.getInputStream(), cachingRequestWrapper.getCharacterEncoding());
//        System.out.println("Request Body: " + requestBody);
//        System.out.println("Request Body: " + cachingRequestWrapper.getContentAsByteArray());
//        System.out.println("Content Length: " + request.getContentLength());

        return true;
    }

    private String readInputStream(InputStream inputStream, String characterEncoding) {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, characterEncoding))) {
            StringBuilder stringBuilder = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }
            return stringBuilder.toString();
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

}

