package com.example.lab3;

import com.example.lab3.api.UsersApi;
import com.example.lab3.exceptions.BadRequestExcep;
import com.example.lab3.exceptions.NotFoundException;
import com.example.lab3.exceptions.UnauthorizedException;
import com.example.lab3.exceptions.UserAlreadyExists;
import com.example.lab3.model.*;
import com.example.lab3.services.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.*;

@Validated
@Controller
public class UsersController implements UsersApi {

    private SecurityService securityService;

    @Value("${usernameAuth}")
    private String userName;

    @Value("${passwordAuth}")
    private String password;

    @Autowired
    UsersController(SecurityService securityService){
        this.securityService = securityService;
    }

    @Override
    public ResponseEntity<UserResponse> createUser( CreateRequest body, String authorizationHeader, String XHMACsignature) throws BadRequestExcep {


        String[] credentials = securityService.extractAndDecodeBasicAuth(authorizationHeader);
        if(credentials == null || !credentials[0].equals(userName) || !credentials[1].equals(password)) {
            throw new UnauthorizedException();
        }

        if(body.getRequestHeader() == null || body.getUser() == null){
            throw new BadRequestExcep();
        }

        if(body.getUser().getId().toString().equals("3fa85f64-5717-4562-b3fc-2c963f66afa6")){
            throw new UserAlreadyExists();
        }

        UserResponse userResponse = new UserResponse();
        userResponse.setResponseHeader(new RequestHeader().requestId(body.getRequestHeader().getRequestId())
                .sendDate(LocalDateTime.now()));
        userResponse.setUser(body.getUser());

        return ResponseEntity.status(HttpStatus.CREATED).body(userResponse);

    }

    @Override
    public ResponseEntity<Void> deleteUser(UUID id, String authorizationHeader) throws NotFoundException {

        String[] credentials = securityService.extractAndDecodeBasicAuth(authorizationHeader);
        if(credentials == null || !credentials[0].equals(userName) || !credentials[1].equals(password)) {
            throw new UnauthorizedException();
        }

        List<User> userList = new ArrayList<>();
        User user = new User()
                .age(12)
                .id(UUID.fromString("3fa85f64-5717-4562-b3fc-2c963f66afa6"))
                .name("Jan")
                .surname("Kowalski")
                .email("e_musk@gmail.com")
                .citizenship(User.CitizenshipEnum.PL)
                .personalId("920111112232");
        userList.add(user);

        ///////////////////////////////////////////////////////////////////////

        User user_found = userList.stream()
                .filter(user_ -> id.toString().equals(user_.getId().toString()))
                .findAny()
                .orElse(null);
        if(user_found != null) {
            userList.remove(user_found);
            return ResponseEntity.noContent().build();
        } else {
            throw new NotFoundException(404, "User not found");
        }

    }

    @Override
    public ResponseEntity<UserListResponse> getAllUsers(String authorizationHeader) {

        String[] credentials = securityService.extractAndDecodeBasicAuth(authorizationHeader);
        if(credentials == null || !credentials[0].equals(userName) || !credentials[1].equals(password)) {
            throw new UnauthorizedException();
        }

        List<User> userList = new ArrayList<>();
        userList.add(new User()
                .age(12)
                .id(UUID.randomUUID())
                .name("Jan")
                .surname("Kowalski")
                .email("e_musk@gmail.com")
                .citizenship(User.CitizenshipEnum.PL)
                .personalId("920111112232")
        );
        userList.add(new User()
                .age(13)
                .id(UUID.randomUUID())
                .name("Jan2")
                .surname("Kowalski4")
                .email("e_musk1@gmail.com")
                .citizenship(User.CitizenshipEnum.PL)
                .personalId("920111115532")
        );

        /////////////////////////////////////////////////////////////

        UserListResponse response = new UserListResponse();
        response.setResponseHeader(new RequestHeader().requestId(UUID.randomUUID())
                .sendDate(LocalDateTime.now()));
        response.setUsersList(userList);
        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<UserResponse> getUserById(UUID id, String authorizationHeader) throws NotFoundException, BadRequestExcep {

        String[] credentials = securityService.extractAndDecodeBasicAuth(authorizationHeader);
        if(credentials == null || !credentials[0].equals(userName) || !credentials[1].equals(password)) {
            throw new UnauthorizedException();
        }

        List<User> userList = new ArrayList<>();
        User user = new User()
                .age(12)
                .id(UUID.fromString("3fa85f64-5717-4562-b3fc-2c963f66afa6"))
                .name("Jan")
                .surname("Kowalski")
                .email("e_musk@gmail.com")
                .citizenship(User.CitizenshipEnum.PL)
                .personalId("920111112232");
        userList.add(user);

        ///////////////////////////////////////////////////////////////////////

        User user_found = userList.stream()
                .filter(user_ -> id.toString().equals(user_.getId().toString()))
                .findAny()
                .orElse(null);
        if(user_found != null) {
            UserResponse response = new UserResponse();
            response.setResponseHeader(new RequestHeader().requestId(UUID.randomUUID())
                    .sendDate(LocalDateTime.now()));
            response.setUser(user_found);
            return ResponseEntity.ok(response);
        } else {
            throw new NotFoundException(404, "User not found");
        }

    }

    @Override
    public ResponseEntity<UserResponse> updateUser(UUID id, UpdateRequest body, String authorizationHeader) throws NotFoundException, BadRequestExcep {

        String[] credentials = securityService.extractAndDecodeBasicAuth(authorizationHeader);
        if(credentials == null || !credentials[0].equals(userName) || !credentials[1].equals(password)) {
            throw new UnauthorizedException();
        }

        List<User> userList = new ArrayList<>();
        User user = new User()
                .age(12)
                .id(UUID.fromString("3fa85f64-5717-4562-b3fc-2c963f66afa6"))
                .name("Jan")
                .surname("Kowalski")
                .email("e_musk@gmail.com")
                .citizenship(User.CitizenshipEnum.PL)
                .personalId("920111112232");
        userList.add(user);

        ///////////////////////////////////////////////////////////////////////

        if (id == null || body.getRequestHeader() == null  || body.getUser() == null) {
            throw new BadRequestExcep();
        } else {
            User user_found = userList.stream()
                    .filter(user_ -> id.toString().equals(user_.getId().toString()))
                    .findAny()
                    .orElse(null);
            if (user_found != null) {

                user.age(body.getUser().getAge())
                        .name(body.getUser().getName())
                        .surname(body.getUser().getSurname())
                        .email(body.getUser().getEmail())
                        .citizenship(body.getUser().getCitizenship())
                        .personalId(body.getUser().getPersonalId());

                UserResponse response = new UserResponse();
                response.setResponseHeader(new RequestHeader().requestId(UUID.randomUUID())
                        .sendDate(LocalDateTime.now()));
                response.setUser(user);
                return ResponseEntity.ok(response);
            } else {
                throw new NotFoundException(404, "User not found");
            }
        }
    }
}
